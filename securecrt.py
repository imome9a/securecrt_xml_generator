#!/usr/bin/env python3
# crafted out of necessessity by Aaron Francis aka "imome9a"

# This script will read in a csv file containing 3 columns
# labeled "folder", "name", and "address" and create an XML
# document that can be imported into SecureCRT

import csv
import os

# function to remove a line by linenumber in a file
def remove_line(fileName,lineToSkip):
    with open(fileName,'r') as rfile:
        lines = rfile.readlines()
    currentLine = 1
    with open(fileName,'w') as wfile:
        for line in lines:
            if currentLine == lineToSkip:
                pass
            else:
                wfile.write(line)
            currentLine += 1

csvFile = input("Provide the full path and file name to the csv file: ")
#csvFile = "/home/aaron/Shared/all_nodes.csv"

# check if file exists
fileCheck = os.path.isfile(csvFile)
if fileCheck == True:
    # read in the csv file and create a sorted dictionay object from it
    with open(csvFile,'r', encoding='utf-8-sig') as csvfile:
        csvreader = csv.DictReader(csvfile)
        csvDict = list(csvreader)
        sortDict = sorted(csvDict, key=lambda x: x['site'])
        #print(type(csvDict))
        #print(sortDict)
    # create the xml file in the same directory by iterating through each row of the csv
    print ("Creating the XML file...")
    xmlFile = "/home/aaron/Shared/scrt.xml"
    with open(xmlFile,'w') as xmlfile:
        print(f'<?xml version="1.0" encoding="UTF-8"?>', file=xmlfile)
        print(f'<VanDyke version="3.0">', file=xmlfile)
        print(f'    <key name="Sessions">', file=xmlfile)
        print(f'        <key name="Network">', file=xmlfile)
        grpInit = ""
        siteInit = ""
        typeInit = ""
        # enter loop
        for entry in sortDict:
            group = entry['group'] # loop
            site = entry['site'] # loop
            type = entry['type'] # loop
            name = entry['name']
            address = entry['address']
            # this creates the folder heirarchy that all related sessions should fall under
            while grpInit != group:
                print(f'                    </key>', file=xmlfile)
                print(f'                </key>', file=xmlfile)
                print(f'            </key>', file=xmlfile)
                print(f'            <key name="{group}">', file=xmlfile)
                print(f'                <key name="{site}">', file=xmlfile)
                print(f'                    <key name="{type}">', file=xmlfile)
                grpInit = group
                siteInit = site
                typeInit = type
            else: # while grp matches
                while siteInit != site:
                    print(f'                    </key>', file=xmlfile)
                    print(f'                </key>', file=xmlfile)
                    print(f'                <key name="{site}">', file=xmlfile)
                    print(f'                    <key name="{type}">', file=xmlfile)
                    siteInit = site
                    typeInit = type
                else: # while site matches
                    while typeInit != type:
                        print(f'                    </key>', file=xmlfile)
                        print(f'                    <key name="{type}">', file=xmlfile)
                        typeInit = type
                    else: # while type matches
                        print(f'                        <key name="{name}">', file=xmlfile)
                        print(f'                            <string name="Hostname">{address}</string>', file=xmlfile)
                        print(f'                        </key>', file=xmlfile)
        # end of loop
        print(f'                    </key>', file=xmlfile)
        print(f'                </key>', file=xmlfile)
        print(f'            </key>', file=xmlfile)
        print(f'        </key>', file=xmlfile)
        print(f'    </key>', file=xmlfile)
        print(f'</VanDyke>', file=xmlfile)
    # remove bogus line generate by entering initial loop
    remove_line(xmlFile,5)
    remove_line(xmlFile,5)
    remove_line(xmlFile,5)
    print ("Your SecureCRT compliant XML file has been created.")
else:
    print(f'You fail, the file you specified "{file}" doesnt exist. Check your path.')
